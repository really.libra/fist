// Mobile navigation menu
function mobileMenu() {
  const html = document.getElementsByTagName('html')[0];
  const body = document.getElementsByTagName('body')[0];
  const toggle = document.getElementById('mobile-menu-toggle');
  const menu = document.getElementById('menu');

  // Toggling mobile navigation menu
  function toggleMobileNav() {
    if (toggle) {
      toggle.addEventListener('click', () => {
        if (toggle.classList.contains('open')) {
          toggle.classList.remove('open');
          menu.classList.remove('is-opened');
          body.classList.remove('menu-is-opened');
          html.classList.remove('overflow-hidden');
        } else {
          toggle.classList.add('open');
          menu.classList.add('is-opened');
          body.classList.add('menu-is-opened');
          html.classList.add('overflow-hidden');
        }
      });
    }
  }

  // Closing mobile navigation menu
  function closeMobileNav() {
    const windowWidth = window.innerWidth;

    if (windowWidth >= 990 && toggle.classList.contains('open')) {
      toggle.classList.remove('open');
      menu.classList.remove('is-opened');
      body.classList.remove('menu-is-opened');
      html.classList.remove('overflow-hidden');
    }
  }

  window.addEventListener('resize', (event) => {
    closeMobileNav();
  });

  toggleMobileNav();
}

// To initialize all Sliders (Swipers)
function setSliders() {
  const sliderElements = document.querySelectorAll('.slider');

  if (sliderElements) {
    sliderElements.forEach(sliderElement => {
      const swiperElement = sliderElement.querySelector('.swiper');

      const swiper = new Swiper(swiperElement, {
        loop: true,
        pagination: {
          el: sliderElement.querySelector('.swiper-pagination'),
        },
        navigation: {
          nextEl: sliderElement.querySelector('.swiper-button-next'),
          prevEl: sliderElement.querySelector('.swiper-button-prev'),
        },
        slidesPerView: 1,
        spaceBetween: 24,
        breakpoints: {
          480: {
            slidesPerView: 2,
            spaceBetween: 24
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 24
          },
          990: {
            slidesPerView: 4,
            spaceBetween: 24
          }
        }
      })
    });
  }
}

// To initialize all Accordions
function setAccordions() {
  const accordionElements = document.querySelectorAll('.accordion');

  if (accordionElements) {
    accordionElements.forEach(accordionElement => {
      const accordionItems = accordionElement.querySelectorAll('.accordion-item');
      
      accordionItems.forEach((item) => {
        const header = item.querySelector('.accordion-header');
        const body = item.querySelector('.accordion-body');

        header.addEventListener('click', () => {
          accordionItems.forEach((acItem) => {
            const acBody = acItem.querySelector('.accordion-body');
            if (acItem !== item && !acBody.classList.contains('collapsed')) {
              acBody.classList.add('collapsed');
              acBody.style.height = '0';
              acBody.previousElementSibling.classList.remove('collapse');
            }
          });
          header.classList.toggle('collapse');
          body.classList.toggle('collapsed');

          if (body.classList.contains('collapsed')) {
            body.style.height = '0';
          } else {
            body.style.height = body.scrollHeight + 'px';
          }
        });
      });
    });
  }
}

// To set lazy loading on all links of 'Our Process' section.
function ourProcessLazyLoading() {
  const ourProcess = document.getElementsByClassName('our-process')[0];
   
  if (ourProcess) {
    let scroll = document.documentElement.scrollTop;
    let ourProcessTop = ourProcess.offsetTop - 150;

    if (scroll >= ourProcessTop && !ourProcess.classList.contains('lazyload')) {
      ourProcess.classList.add('lazyload');
    }
  }
}

window.addEventListener('scroll', (event) => {
  ourProcessLazyLoading();
});

window.addEventListener('load', (event) => {
  mobileMenu();
  setSliders();
  setAccordions();
  ourProcessLazyLoading();
});
